---
path: 'jouke-ypma'
name: 'Jouke Ypma'
tagline: 'Sorry, Didnt mean to enjoy the moment.'
avatar: './avatar.jpg'
role: 'developer'
facebook: ''
github: ''
gitlab: 'yowkah'
linkedin: ''
slack: 'yowkah'
soundcloud: ''
twitter: ''
website: ''
---

# Oh, hello!

I'm a full stack javascript enthousiast from the Netherlands. I'm a system engineer by trade but I also build and maintain internal tooling which is primarily written in ECMAscript (let's be politically correct here) and VB.net. I don't have any specialties in the back- or frontend but I like messing around with both.

For now I'm focussing on data visualization with D3.js and getting started with collaborative work.

## Libraries I like to work with

- Express
- Mongo DB
- Vue, Vuex, Vue-router
- Pug
- spectre.css
- Puppeteer
- Jest

## other stuff i like

- Arch Linux
- Sublime Text 3
- Mechanical keyboards
- Online games
- The paid version of Winrar

# any questions?

Let me know on Slack or gitlab (@yowkah)
