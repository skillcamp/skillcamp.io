---
path: 'balaji-jagadeesan'
name: 'Balaji Jagadeesan'
tagline: 'This guy is Meh!!!'
avatar: 'baj_profile_pic.jpg'
role: 'developer'
facebook: ''
github: 'BalajiJagadeesan'
gitlab: 'BalajiJagadeesan'
linkedin: 'balaji-jagadeesan'
slack: 'Baj'
soundcloud: ''
twitter: ''
website: ''
---

Hey fellow developers,

I'm a recent grad working towards to become a full-stack developer.
Looking forward to contribute as well as learn from the community.

###What interests me?

- Backend development
- Hybrid mobile application development
- Front end with pure javaScript

### What I use?

- JavaScript
- Node.js
- React-Native
- GraphQL
- HTML5/CSS3
- React

### Learning

- Rust

### Side activities/ interests

- Home automation and networks
- Exploring new places
- Hiking
- Long Drives
