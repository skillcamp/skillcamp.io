---
path: 'david-omar'
name: 'David Omar'
tagline: "I'm a Laker fan now #bandwagon"
avatar: './davidomar.jpg'
role: 'developer'
facebook: ''
github: ''
gitlab: 'rankupdavid'
linkedin: ''
slack: 'David Omar'
soundcloud: ''
twitter: 'rankupdavid'
website: ''
---

# Hi there!

I am a self taught developer in Miami, FL. My main language I code in is Javascript.
I do frontend development but highly interested in backend. I am always striving to get better
at building web applications. I enjoy working with others so please reach out if you ever want
to pair program or talk something out.

### Technology I Use/Want To Learn

- Javascript
- HTML/CSS(SASS)
- React
- Progressive Web Apps
- Redux
- NodeJS
- MongoDB
- SQL
