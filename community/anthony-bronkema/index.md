---
path: 'anthony-bronkema'
name: 'Anthony Bronkema'
tagline: 'Family, Beer, Coffee and JavaScript <3'
avatar: 'anthony-bronkema.jpg'
role: 'developer'
github: 'anthonybronkema'
gitlab: 'anthonybronkema'
twitter: '@anthonybronkema'
---

# Hey there

I'm loving everything I'm learning about Javascript. I'm hoping to extend this to focus more on backend/data work as I am WAY more comfortable with logic over aesthetics and design.

## My Favorite Things

- <a href="https://rhinegeist.com/" target="_blank">Rhinegeist Truth IPA</a>
- <a href="https://javascript30.com/" target="_blank">Wes Bos' Javascript30 Course</a>
- <a href="https://syntax.fm/" target="_blank">Syntax.fm Podcast</a>
- <a href="https://dev.to/" target="_blank">Dev.to</a>
