---
path: 'cristina-ruth'
name: 'Cristina Ruth'
tagline: 'Full-Stack Web Developer'
avatar: 'cristina-ruth.jpg'
role: 'mentor'
facebook: ''
github: ''
gitlab: ''
linkedin: ''
slack: 'Cristina'
soundcloud: ''
twitter: ''
website: ''
---

Hi everyone. I have been a full-stack web developer for about 5 years now. I help add new features to websites, release them as stable functionalities and also support operations in ensuring our websites stay up and running smoothly. Still always learning something new everyday!

I have started coding 20 years ago and have always enjoyed the problem-solving challenges it presents. I'm always happy to help , so please holler if you need anything.

## Technologies

- .NET C#, MVC
- HTML / CSS
- javascript / jQuery
- SQL

## Things I'm Learning on the Side

- React (loving it)
