---
path: 'simonas-daniliauskas'
name: 'Simonas Daniliauskas'
tagline: '#Software developer'
avatar: 'simonas-daniliauskas.jpg'
role: 'developer'
facebook: 'simonas.daniliauskas'
github: 'simdani'
gitlab: ''
linkedin: 'simonas-daniliauskas-3b685a74'
slack: ''
soundcloud: ''
twitter: ''
website: 'https://simdan.lt'
---

# Hi!

I'm currently studying computer science and trying to learn new technologies and skills. I believe that the best way to learn new things is to make something
with other people, so that's why I joined Skillcamp!

### My favorite tech

- C#
- Javascript (Node.js, React)
- Java
