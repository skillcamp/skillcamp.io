---
path: 'peregrin-garet'
name: 'Peregrin Garet'
tagline: 'Fullstack Web Developer'
avatar: 'peregrin-garet.png'
role: 'developer'
facebook: ''
github: ''
gitlab: 'pgaret'
linkedin: ''
slack: 'Peregrin'
soundcloud: ''
twitter: ''
website: ''
---

# Currently Doing:

- 9-5 is Jr. Frontend Engineer at a tech consulting firm in NYC, working primarily with React
- Building a game website as a side-project with a friend
- Excited to add more items to this list!

# Familiarities

- React / Redux
- Electron
- JavaScript
- AngularJS (the OG)
- Basic APIs (I've worked with APIs built in Ruby, Python, JavaScript, & PHP)
- SQL
- Generic Git Stuff

# I'm interested in

- Contributing to projects
- Meeting other developers

# A bit about myself.

I'm a 26 year old web developer living in NYC. I grew up in sunny SoCal and went to school upstate for Business & Game Development. I moved to the city immediately after for a job in QA, but wanted to be more coding-heavy so I attended a bootcamp to learn web dev. 2 years later, here I am making websites for a living. When not programming, I like board games and concerts.
