---
path: 'guido-scalise'
name: 'Guido Scalise'
tagline: 'Systems Architect and Software Engineer'
avatar: './guido.jpg'
role: 'mentor'
facebook: ''
github: 'gscalise'
gitlab: 'guido_s'
linkedin: 'guidoscalise'
slack: 'Guido'
soundcloud: ''
twitter: 'gscalise'
website: ''
---

Hi! I'm a software architect, designer and developer from Argentina, based in the UK. I've been programming since I was a child, and during my career I've worked in all sorts of projects and technologies. I love complex challenges, especially troubleshooting, performance analysis and reverse engineering. I'm also maniac about containerization, modularity, security, reusability, scalability and fault tolerance.

### My Favourite Technologies

- Java
- JavaScript
- TypeScript
- Python
- Bash
- Docker & Kubernetes
- ELK Stack
- Linux

### My Favourite Subjects

- Systems design
- Data Structures
- Functional and OO Programming
- Distributed, highly scalable, fault tolerant systems
- Cloud infrastructure and automation
- InfoSec
- Machine Learning & Deep Learning, particularly for automated anomaly detection
- Biomedical Engineering
- 2D/3D graphics
- Math, Physics and Applied Sciences
