---
path: 'jeremy-randall'
name: 'Jeremy Randall'
tagline: '#Software developer'
avatar: 'deremije.jpg'
role: 'developer'
facebook: ''
github: ''
gitlab: ''
linkedin: 'jeremy-randall-61217919'
slack: 'Jeremy'
soundcloud: ''
twitter: ''
website: 'http://www.deremije.com'
---

#Hi

I'm a self-taught developer looking for interesting projects to work on. I work in HTML, CSS, JavaScript, Node, React, Python, and whatever you need me to learn in order to code.
