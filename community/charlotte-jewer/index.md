---
path: 'charlotte-jewer'
name: 'Charlotte Jewer'
tagline: 'I like cats & beer.'
avatar: 'charlotte-jewer.jpeg'
role: 'developer'
facebook: ''
github: 'Charlotte990'
gitlab: 'Charlotte990'
linkedin: 'charlotte-jewer'
slack: 'Charlotte'
soundcloud: ''
twitter: ''
website: 'https://charlotte990.gitlab.io'
---

#Hello 👋

## Things I'm currently learning:

- JavaScript
- VueJS
- Go
- GraphQL

### My favourite online learning resources:

- <a href="https://vueschool.io/" target="_blank">VueSchool</a>
- <a href="https://www.codecademy.com/learn" target="_blank">Codecademy</a>
- <a href="https://exercism.io" target="_blank">Exercism</a>
- <a href="https://gophercises.com/" target="_blank">Gophercises</a>
- <a href="https://www.codewars.com/" target="_blank">CodeWars</a>

#### And this is my dog:

![Imgur](https://i.imgur.com/3DyRTQ9.jpg?2)

🤙
