---
path: 'prashant-chaudhari'
name: 'Prashant Chaudhari'
tagline: 'I love mountains and to build things for people.'
avatar: 'prashant-chaudhari.jpeg'
role: 'developer'
facebook: ''
github: 'pandaa880'
gitlab: 'pandaa880'
linkedin: ''
slack: 'Prashanth Chaudhari'
soundcloud: ''
twitter: 'pandaa880'
website: ''
---

<!--- Share anything you would like to about your developer experience -->
<!-- Updated my index.md file -->

# Hey There ✌

# Things I am currently learning & improving

- React
- Advance Node.js
- GraphQL
- Advance CSS

# Future tech and things which I am excited to learn

- AR/VR Development
- Blockchain
- Deep Learning & AI

# What I am learning extra ?

- Basic Computer Science Subjects.

# A bit about myself.

I am from Ahmedabad, India. I am a drop out and self taught developer. Currently I am all into JavaScript but later I am planning to go into Deep Learning and AI. I am here to learn and help fellow developers. As a self taught dev many people have helped me and I want to help others as well. I have started contributing to open source and so far it is very amazing experience for me. I am making my way through **FreeCodeCamp** for now and actively looking for projects to work on.

# Places where I hang out...

- [**FreeCodeCamp**](https://www.freecodecamp.org/pandaa880)

And bunch of **Slack** and **Discord** channels.
