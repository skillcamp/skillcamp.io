---
path: 'johan-axelsson'
name: 'Johan Axelsson'
tagline: 'Frontend Developer'
avatar: './johan-axelsson.jpg'
role: 'mentor'
facebook: 'nerdic.coder'
github: 'nerdic-coder'
gitlab: 'nerdic-coder'
linkedin: 'johax'
slack: 'Johan Axelsson'
soundcloud: ''
twitter: 'nerdic_coder'
website: 'https://nerdic-coder.com/'
---

Y'ello!

I'm a Frontend Application Developer on Åland Island, Finland.
I work at a company called Crosskey and I'm in the team that develop mobile banking applications.
Our tech stack includes Angular, Ionic, Cordova, NodeJS, Git, Jenkins and Java.

Just hit me up on any social channel that I'm available at if you need any help with frontend development issues or questions.

### Top Languages

- Javascript
- Typescript
- Java
- PHP
