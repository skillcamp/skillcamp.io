---
path: 'asif-kabani'
name: 'Asif Kabani'
tagline: 'Front End Developer & UI Designer'
avatar: './asif.jpg'
role: 'developer'
facebook: ''
github: 'asifkabani'
gitlab: 'asifkabani'
linkedin: ''
slack: 'Asif Kabani'
soundcloud: ''
twitter: ''
website: 'https://asifkabani.com/'
---

Hi I am Asif Kabani, a Front End Developer with expertise in HTML/CSS & JavaScript. Also I am aspiring to be a UI Designer.

### Technologies

- HTML/CSS
- JavasSript
- React
- Bash
- Adobe XD, Sketch, Invision
