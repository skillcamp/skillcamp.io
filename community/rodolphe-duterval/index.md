---
# required fields
path: 'rodolphe-duterval'
name: 'rodolphe'
tagline: 'Tech Enthusiast & bad Humorist'
avatar: './image-rodolphe.jpg'
role: 'developer'
gitlab: 'bu7ch'

# optional fields
facebook: ''
github: 'bu7ch'
linkedin: 'rodolpheduterval'
slack: 'bu7ch'
soundcloud: ''
twitter: '_notbu7ch'
website: ''
---

# Hello 👋

I'm a fullstack javascript/ruby enthousiast from France.

## My Favourite Technologies

- JavaScript
- TypeScript
- Ruby
- Angular 7
- React
- Node
  ...etc
