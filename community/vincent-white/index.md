---
# required fields
path: 'vincent-white'
name: 'Vincent White'
tagline: 'A Hobbyist Coder'
avatar: 'avatar.jpg'
role: 'mentor'
gitlab: 'xstrengthofonex'

# optional fields
facebook: 'codelivebroadcasts'
github: 'xstrengthofonex'
slack: 'xstrengthofonex'
website: 'https://xstrengthofonex.github.io/'
---

## About Me

My name's Vince. I'm currently teaching in South Korea. I got into programming a few years ago.
Started off with Code Academy learned some Javascript and then moved on to the full-stack web development course on Udacity.
I've tried lots of different languages along the way, but always keep coming back to Python: My first love.
I more recently became interested in the principles of Software Craftsmanship.
My hobbies asides from coding are reading, writing songs and drawing.
I'm looking forward to working on some projects here and building some awesome stuff.
If anyone's interested in doing some pair programming hit me up on slack.

## My Favorite Programming Books

1. Test-driven Devlopment by Example - Kent Beck
2. Clean Code - Robert C. Martin
3. The Software Craftsman: Professionalism, Pragmatism, Pride - Sandro Mancuso
4. Growing Object-Oriented Software, Guided by Tests - Nat Pryce and Stevce Freeman
5. Patterns of Enterprise Application Architecture - Martin Fowler

## My Favorite Languages

1. Python
2. Java
3. Go
4. Javascript
5. C
