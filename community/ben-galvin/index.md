---
path: 'ben-galvin'
name: 'Ben Galvin'
tagline: '✌️'
avatar: 'ben-galvin.jpeg'
role: 'developer'
facebook: ''
github: ''
gitlab: 'galvin.ben'
linkedin: 'ben-galvin-92683a45'
slack: 'Ben'
soundcloud: ''
twitter: ''
website: 'https://ben-galvin.co.uk/'
---

# Hello.

Having been a tester for the last few years and craving development work, I have been slowly pushing myself into a developer role. I have made my way through a lot of tutorials online and am now beginning to contribute towards real projects, starting with Skillcamp! 🎉

### Code

I have played with a lot of different languages and frameworks, but these are my current focus:

<div style="display:flex;flex-direction:row;justify-content:space-between">
  <div style="display:flex;flex-direction:column;align-items:center;">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Gogophercolor.png/240px-Gogophercolor.png" style="width:100px;height:100px;"> 
    Go
  </div>
  <div style="display:flex;flex-direction:column;align-items:center;">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png" style="width:100px;height:100px;">
    Javascript
  </div>
  <div style="display:flex;flex-direction:column;align-items:center;">
    <img src="https://vuejs.org/images/logo.png" style="width:100px;height:100px;">
    Vue.js
  </div>
</div>
<br>

### Hobbies

My hobbies outside of programming include:

🚲 Cycling  
🥖 Baking  
⚒ Woodwork  
🧗‍♂️ Climbing  
🏕 Camping  
🤸‍♂️ Yoga  
🥦 Gardening  
📚 Reading
