---
path: 'anthony-rivas'
name: 'Anthony Rivas'
tagline: 'Full-Stack Developer and Teacher'
avatar: './profile-image.jpg'
role: 'mentor'
facebook: ''
github: 'anthonyrivas'
gitlab: 'anthonyrivas'
linkedin: ''
slack: 'TJ'
soundcloud: 'binary-view'
twitter: ''
website: 'http://aerivas.com/'
---

Hey everyone. I am a full-stack developer and also an instructor for the University of Arizona Coding Bootcamp. I have a passion for well written, maintainable, and beautiful JavaScript (Just don't verify this with my hacking around on Github 😂).

When I'm not coding, I can be found with my family, listening to music, and playing videogames.

I am also the co-host of a realtively new , and very inconsistently recorded podcast called K&&R: BinaryView

### Top Technologies

- JavaScript / Node
- HTML / CSS
- React
- PHP
- Azure SQL
- MongoDB
