---
path: 'alfredo-colon'
name: 'Alfredo Colón'
tagline: 'Cloud Software Engineer'
avatar: './avatar.jpg'
role: 'mentor'
facebook: ''
github: ''
gitlab: 'MarketingLinkCTO'
linkedin: ''
slack: 'Alfredo'
soundcloud: ''
twitter: ''
website: ''
---

Passionate software engineer that loves to teach and learn.
