const path = require('path')

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPlugin({
    name: 'babel-plugin-transform-regenerator',
  })
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const blogPostTemplate = path.resolve(`src/templates/blog-post.js`)
    const communityMemberTemplate = path.resolve(
      `src/templates/community-member.js`
    )
    resolve(
      graphql(
        `
          {
            blogPosts: allMarkdownRemark(
              filter: { fileAbsolutePath: { regex: "//blog/.*/" } }
            ) {
              edges {
                node {
                  html
                  id
                  frontmatter {
                    date
                    path
                    title
                    excerpt
                  }
                }
              }
            }
            communityMembers: allMarkdownRemark(
              filter: { fileAbsolutePath: { regex: "//community/.*/" } }
            ) {
              edges {
                node {
                  html
                  frontmatter {
                    name
                    role
                    tagline
                    path
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          reject(result.errors)
          console.log(result.errors)
          return
        }
        result.data.blogPosts.edges.forEach(({ node }) => {
          createPage({
            path: `/blog/${node.frontmatter.path}`,
            component: blogPostTemplate,
            context: {
              _path: node.frontmatter.path,
            },
          })
        })
        result.data.communityMembers.edges.forEach(({ node }) => {
          createPage({
            path: `/community/${node.frontmatter.path}`,
            component: communityMemberTemplate,
            context: {
              _path: node.frontmatter.path,
            },
          })
        })
      })
    )
  })
}
