---
path: 'what-is-skillcamp'
date: '2018-08-13T00:00:00+00:00'
title: 'Skillcamp: A community by developers for developers'
author: 'Skillcamp'
avatar: '../avatars/skillcamp.png'
excerpt: 'What is Skillcamp and why you should stop learning
  web development on your own'
coverImage: './skillcamp_cover.jpg'
thumbnail: './skillcamp_square.jpg'
tags: ['skillcamp']
---

Learning the ins and outs of web developments can oftentimes feel like a solitary task, but we would like to challenge that paradigm. We are more connected than ever and would like to see this translate into the learning process. Having a like-minded community to help you along the way can expedite the process of becoming an ace developer. We believe that Skillcamp is a better way to learn and would like to share that with as many people as possible.

###What is Skillcamp?

We are a group of developers who aim to improve our skills through building projects, connecting with others, and establishing a friendly and welcoming web development community.

Skillcamp is unique in that our culture encourages trying new things, making mistakes and learning along the way. Its members all have the same goal in mind, so you never have to feel bad for not knowing something or asking questions. We aim to guide, encourage and push each other to be the best that we can be.

###Our Story

Our group started on Reddit in early June 2018. A post went up asking developers if they would be interested in working on a project together, to gain the skills required to work with a team. It turns out this resonated with quite a few early members, and we soon started our first project.

Over the next couple of weeks, we had grown to around 50 members. We quickly realized that our one-off Reddit project had become bigger than we originally planned. After some discussion, we decided to create a community that centered around this idea of team-driven and project-based learning.

But, the most exciting part is that Skillcamp’s story is still largely unwritten. Our developers are in the driver's seat, with as much influence over the direction of the community as they would like to invest. We are still in the formative stages as a community, but we are excited and hopeful for the future.

###Core Values

Although Skillcamp is free to grow in whichever direction best serves its developers, we do operate on a set of core values that, we believe, are integral to the community.

- **Build projects** - Skillcamp’s effectiveness centers around collaboration. To get the most out of Skillcamp, get involved with a project. If you don’t see something that excites you, please don’t hesitate to suggest new projects or features of your own. The main goal is to plan, discuss and solve problems WITH other developers.

- **Uphold a constructive culture** - Seek to help out members when you can. You may not be an expert, but you will always be a relative expert to someone, so share your knowledge! Be encouraging and empathetic to other developers. Be the type of mentor that you would want to have. On the flip side, always give a problem your honest try beforehand, but never be afraid to seek help when you need it.

- **Transparent community decisions** - Skillcamp is a community built for developers, by developers. We are not owned by a single source, but rather are driven by passionate developers who step into leadership roles. We invite and encourage anyone to participate in shaping the future of the community.

- **Be bold and improve your skills** - Our goal is to create a platform for developers to learn and grow together. We encourage our members to step outside of there comfort zones. Try new technologies, languages, or roles. If you are a backend engineer, interested in front-end design, then give it a shot! Don’t be afraid to make mistakes. You will become a better developer from it.

###Stop learning on your own

If you haven’t already, become part of our growing community of developers from around the world. It is an exciting time to become part of Skillcamp and we would love to have you as a member. Your unique skills and experiences will surely help shape the future of the community for the better.

Check out our [Join Us Page](/join) to get started today!
