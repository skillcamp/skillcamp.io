---
path: 'jordan-hensley/zero-to-employed'
date: '2018-08-08T00:42:27+00:00'
title: 'Self-taught Developer: From Zero To Employed In One Year'
author: 'Jordan Hensley'
avatar: '../../community/jordan-hensley/jordan.jpg'
excerpt: 'My personal story as a web developer. How I went from knowing
  nothing about code to landing my first tech job.'
coverImage: './developer.jpg'
thumbnail: '../avatars/jordan.jpg'
tags: ['community', 'career']
---

As a beginner with no coding experience, it's an understatement to say that the industry seems intimidating. Every blog post you read is going to give you a different opinion on the best way to get started, complete with its own assortment of tech jargon that may or may not mean anything to you at this point. The goal of this article is not to tell you what to do, but rather, give you some milestones to look for in your first year of programming.

I'm going to outline my progress, from total beginner to my first job as a web developer. When you're starting out, it's hard to know where you stand as a developer, and when you are ready to start applying for jobs. I am hoping that my progress will give you a little perspective on where you are and what's to come.

## A little about me

I'm not an expert code guru, and I haven't built apps for any huge tech companies, but I did recently land my first tech job! And I think this kind of makes me uniquely qualified to give some insight to those of you who are just starting your coding journey.

I still remember, very clearly, what it's like to be completely overwhelmed by programming. It seems like there is an endless amount of things you need to learn just to build an embarrassingly simple website.

Eventually, everything does get easier. It will take time, but before you know it you will be surprised at what you're able to do. Keep at it!

###Disclaimer

My journey isn't necessarily going to look exactly the same as yours. Some of you will pick up on things a lot quicker than I did, or some of you may not have as much time to spend on programming. You may use different programming languages or do things in a different order. Nevertheless, I hope you can find something useful or relatable.

![starting-out](jump_in.jpg)

##Like an array, I started from zero ( Month 0 )

My first introduction to programming was a C++ class that I took in college. I didn't hate the class, but it was definitely not something that I would work on outside of the assigned work. The whole class could be summed up as "how to make a glorified calculator". There was no user interface, everything was done in the console, and I never made anything I would actually use as a consumer.

Not knowing anything about the software industry, I remember thinking, "Ok, I did well in this class. Am I a developer now?". The answer was almost certainly no, but at that point, I had no concept of what a programmer did on a day to day basis. For all I knew, people paid programmers to write these lame little calculators.

To the credit of C++, I think this did give me a foundation for some universal computer science concepts. Variables, loops, data types etc. If you are in the position of choosing a first programming language, seriously, pick any of them. It doesn't matter. You are going to come across a ton of languages throughout your career. It's almost guaranteed that you won't be using the language you started out learning. Once you have some of these fundamental concepts down, picking up a new language is just a matter of syntax.

### Milestones

- Prompt user input and print things to the console.
- Functions that could add numbers.
- Some basic loops.

## Coding for fun ( Month 1-3 )

Once my class was over, I started looking for other resources to learn more about programming. I stumbled across the Harvard CS50 course on Youtube. Although I didn't fully understand a lot of the lecture material, I was able to follow along with some of the global concepts.

I saw that they were starting to use Python instead of C. I had heard of Python before, so I figured I'd give it a shot.

What a breath of fresh air, after coming from a low-level language!
Everything seemed easier to do and faster to write. I think this ultimately lead me to actually enjoy writing code.

My preferred way of learning was to watch Youtube videos. I would follow along with a project or find tutorials whenever I would get stuck or needed to learn something new.

I would also try to work through some of the coding challenge sites like Project Euler or Hacker Rank. Usually, I wouldn't be able to complete more than the first few beginner challenges, but this was still great practice.

Eventually, I got tired of building things in the console. I wanted to build something that looked nice and was fun to use.

I ended up finding a Python game tutorial and followed along to build my own game featuring my dog, Chase. Although it was far from the next AAA title, this was the first time I had made something that I was actually sort of proud of.

### Milestones

- Use slightly more interesting logic and programming concepts.
- Build simple games that ran in the console.
- Began to think more programmatically.
- Started exploring how to move away from the console.

## How do I share the things I made? ( Month 3 -5 )

After telling a some friends and family about putting my dog in a game, a few people asked to see it. Unfortunately, the only way I knew how to show them, was to run it on my laptop. I really just wanted to be able to send them a link and have them be able to play it on their phone or computer.

After looking into it, I read something about Javascript and making web applications. I didn't know anything about the web or how browsers worked.
The more I read, I began to realize that I had a long road ahead of me. HTML,
CSS, Javascript, Frameworks, Libraries, Domains, Servers, just to name a few.

I decided to take it a step at a time. I ended up buying the book series by John Duckett: HTML & CSS and Javascript & JQuery. I highly recommend these books to those looking to learn the basics of web development, as they're visually appealing and easy to follow along as a beginner.

After a few weeks with HTML & CSS, I felt comfortable enough to start working on a webpage. At this point, I was starting to get more comfortable using StackOverflow, finding solutions to problems as they would come up. Looking back this was a pretty big step in my progress as a developer. Instead of seeing these issues as dead-end roadblocks, I realized debugging and seeking out solutions was all part of the process.

It was probably a full month after picking up those web development books that I deployed my first webpage. It was a very basic design, with a single page and I was using FTP to manage the server. Nonetheless, I was pretty proud of my self.

Shortly after I was able to re-write the game featuring my dog in Javascript, and send out links to friends and family, who all got a kick out of it.

### Milestones

- Render and style simple HMTL elements on a webpage.
- Adding very simple javascript to my web pages.
- Use Google and StackOverflow to find solutions.
- Deploy my first live site to the web.

![chases_trash_adventure](./chases_trash_adventure.gif)

## You'll pay me to do this? ( Month 5-8 )

As I got more comfortable with web development, I would often try to recreate things that I saw on other websites. If I saw a cool UI element or animation, I'd take a peek at the developer console and try to get a sense of how they did it. I think this really helped me improve my eye as a designer. Everything I built was better than the last.

One day I was talking to a family member about learning to make websites, and she mentioned that her church's food pantry was planning on getting a website, and wanted to know if I was interested in building it. This is something that I was already doing in my free time, so I was thrilled when she said they would pay me for it.

Making a website for a client was something I had no experience with, so this was an important learning opportunity for me. I developed better design planning skills and, for the first time, really thought about how users were going to interact with the site.

Shortly after the website, I was asked if I could make a database, for the food pantry, to replace the pen and paper method they used to keep track of customers. At that point, I didn't have a lot of experience with CRUD applications or databases, but I agreed to build it.

Now, I was getting pretty comfortable learning new technologies. I bought a Udemy course on React & Firebase and was quickly starting to jump to the documentation when I needed to look something up.

At this time, I debated whether I was ready to start applying for jobs. I decided against at the time, but looking back this is when I probably should have started applying. Even if I wasn't quite ready for a junior developer role, I think I would have been by the time I landed a job.

### Milestones

- Started using frameworks and libraries.
- Documentation was becoming my preferred resource.
- I was more comfortable using chrome's developer tools.
- Landed my first freelance project.
- Started thinking about applying to junior developer jobs.

## Wow, I can build some pretty cool stuff ( Month 8 - 10)

Soon, I picked up another freelance client, and I am breezing through HTML and CSS elements, used to take me hours. I'm getting comfortable with persisting data, using databases, and hitting 3rd party API's. I am noticing that all of the things that used to be a struggle are now second nature.

I am thinking less about what I need to type to make something work, and more about how to put all the puzzle pieces together. I'm taking into consideration things like performance, file sizes, UI and UX, how maintainable my code is and more.

Now, I am genuinely excited about my projects. I'm not making lame calculators as an exercise anymore. I am building applications that actually solve problems for myself and others. Things that I might actually use as a customer.

I think this was the flipping point for me, in terms of feeling confident about applying for jobs. Even if I didn't have every single requirement in the job descriptions, I often had experience with similar tech, and I knew that I could learn anything I needed to.

### Milestones

- Using and persisting data in my applications.
- Using Git and modern workflows.
- Solving problems, rather than doing coding exercises.
- Thinking about the application as a whole, rather than just worrying about syntax.
- Started applying for jobs & talking to recruiters.

## SkillCamp: Working with other developers ( Month 10 -12 )

One thing that I felt I was missing was experience working in a team. As a self-taught developer, I really didn't have any mentors or coworkers to bounce ideas off and learn from.

This leads to the idea of reaching out to developers on Reddit. I figured there would be at least one person in the same boat as I was, looking to gain experience working with a team. It turns out quite a few people resonated with this idea, which ultimately became SkillCamp!

Working on SkillCamp other developers was a huge step for me. Not only did I start to develop some effective workflow techniques, but also got my first glimpse into the day to day as a software developer.

Participating in code reviews, reading and understanding other people's code, planning and discussing our projects were all invaluable to me as a developer.

### Milestones

- Problem-solving with a development team to find the best solution, rather than the first solution.
- Reading and understanding other developer's code.
- Learning effective team-based workflows.
- Finding myself in a position to help and guide other developers.

## Landing the job ( Month 12 )

Initially, I tried the shotgun method with job applications. The goal was to apply to as many jobs as possible, then hopefully I could land a few interviews. This wasn't as fruitful as expected.

Almost all of my interview opportunities came from recruiters on LinkedIn. I quickly realized that talking with a real person, rather than sending an application to some email address is a much better strategy.

In my experience, interviews are not as bad as anticipated. Everyone I have interviewed with has been really nice. It never felt like they were waiting to catch my mistakes, but rather, we would work or talk through a problem together just like I would expect to do on the job.

After about 4 months of job applications and interviews, and 1 year after my first line of code, I landed my first Job!

### Milestones

- Becoming more comfortable in interviews.
- Being able to effectively pitch and display my skills as a developer.
- Landing a Job!

## Final thoughts

Like I mentioned in the disclaimer, you may or may share the same milestones that I had. You'll almost certainly have different challenges, opportunities and successes. But if you are just starting out, and have no idea what to expect, I hope that my story could at least give you a little glimpse of what's to come.

The road to becoming a web developer isn't an easy one. There are going to be a lot of challenges along the way. At some point, you will probably feel like you will never learn enough or be good enough. Trust me, I've been there (And STILL have these thoughts from time to time).

But it does get easier. Keep making mistakes. Keep learning new things. Keep writing code. Before you know it, you'll look back at where you started, and be amazed at how far you have come!
