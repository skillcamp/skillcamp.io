---
path: 'jordan-hensley/react-state-2019'
date: '2019-02-27T00:42:27+00:00'
title: 'Using React state in 2019'
author: 'Jordan Hensley'
avatar: '../../community/jordan-hensley/jordan.jpg'
excerpt: 'With the introduction of hooks, we have more options than ever to
  use state in React!'
coverImage: './react.jpg'
thumbnail: './react_thumbnail.jpg'
tags: ['react', hooks]
---

Up until React v16.8 the only options for state management were to use a class-based component, or if you wanted to get fancy, incorporate a state management library like Redux or Mobx. Now we have even more options with hooks for functional components.

Let's take a look at how we have traditionally kept state in react, then some of our new options with hooks.

## Class based component

```jsx
import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'

class ConstructorExample extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      count: 0,
    }

    this.increase = this.increase.bind(this)
  }

  increase() {
    this.setState(prevState => ({ count: ++prevState.count }))
  }

  render() {
    return (
      <Fragment>
        <p>{this.state.count}</p>
        <button onClick={this.increase}> + </button>
      </Fragment>
    )
  }
}

ReactDOM.render(Example, document.getElementById('root'))
```

If you have developed in React before, then you are familiar with this pattern.

- Our class contains a constructor function where we can set up or initial state object, as well as take care of any necessary actions on creation.
- Any time we create methods that reference `this` we must bind the proper `this` namespace.
- When updating our state, we use `setState` and pass changes to the state object we wish to make
- Referencing state in our render method requires object traversal to get to the data we are looking for.

Although there are methods to streamline some of these common complexities such as transform-class-properties, this is the way most of us have become accustomed to handling state in React. There is nothing broken about this pattern, and don’t worry class components aren’t going anywhere! However, we have been given some more options with the release of React functional hooks!

Let’s take a look at how this example could be written as a functional component!

## useState

**note:** _Fragment shorthand available with Babel 7+_

```jsx
import React from 'react'
import ReactDOM from 'react-dom'

function Example() {
  const [count, setCount] = React.useState(0)

  return (
    <>
      <p>{count}</p>
      <button onClick={() => setCount(count + 1)}> + </button>
    </>
  )
}

ReactDOM.render(Example, document.getElementById('root'))
```

Wow, off the bat we can see that we have retained identical functionality with half the lines of code. The hooks API abstracts all of the boilerplate needed to manage state into a single line where we call `React.useState()`

Let's take a closer looks.

```jsx
const [count, setCount] = React.useState(0)
```

#### useState()

- `useState` can be passed any data type as an initial state value.
- `useState` returns an array with the first element being the current state, and the second a function to set that state.
- `useState` can be used as many times as needed to create multiple independent state containers.

#### What's with the square brackets?

You may be familiar with object destructuring, where you can grab keys off of an object and store them as variables. The same can be done with arrays. This gives us the freedom to name these values whatever we would like to fit semantically within our component. In our case, we chose `count` and `setCount` respectively, as it accurately describes this state instance.

#### count

Since this is just a variable declared in our function we have access to the `count` state without having to traverse a `this` namespace. We can simply reference the current state with the variable `count`

#### setCount()

`setCount` is a setter function specific to its state instance. Whenever you need to change state, call this function and pass it the new state.

**Note:** Our setter function works a little differently than it did in class components. Unlike `this.setState​` in a class component, `setCount`​ doesn’t do anything fancy like object differencing for you. Whatever you pass into the function is what the new state will be. You can still accomplish similar functionality with objects, however, you will have to spread the current state into a new object, then include any updates manually.

## useReducer

```jsx
import React from 'react'
import ReactDOM from 'react-dom'

const countReducer = (state, action) => {
  const quantity = action.by ? action.by : 1

  switch (action.type) {
    case 'increase':
      return state + quantity
    case 'decrease':
      return state - quantity
    default:
      return state
  }
}

function Example() {
  const [count, dispatch] = React.useReducer(countReducer, 0)

  return (
    <>
      <p>{count}</p>
      <button onClick={() => dispatch({ type: 'increase', by: 5 })}> +5 </button>
      <button onClick={() => dispatch({ type: 'decrease' })}> - </button>
    </>
  )
}

ReactDOM.render(Example, document.getElementById('root'))
```

#### useReducer()

- `useReducer` should be passed a reducer function, initial state, and optionally an initialization function.
- `useReducer` returns an array with the first element being the current state, and the second a dispatch function that takes an input and runs your reducer to tell your state how to change.
- `useReducer` can also be used as many times as needed to create multiple independent state containers.

#### reducer function

The reducer function will tell your state how to change based on what it is given. It is passed a first parameter of the current state or initial state obtained from the `useReducer`​ call, and a second parameter as an action obtained from your `dispatch` function. Your reducer function should make any state manipulations needed then return your new desired state.

#### count

Similar to `useState`​, ​`useReducer`​ returns the current state as the first array element, which could be any data type. However, since ​`useReducer`​ will be used for more complex state manipulation, this will commonly be an object or array.

#### dispatch()

Although you are free to name this as you please, you will commonly see this named as some variation of "dispatch", to mimic patterns found in Redux. This function takes an argument that gets passed to the action parameter of your reducer function. Again, to follow the patterns established by Redux, you will most likely see the dispatch argument as an object with a key of `type`, to determine which action to take, as well as any other relevant data.

```jsx
dispatch({
  type: 'update',
  ...data,
})
```

## When should you use each method?

Honestly, it really comes down to what works best for your situation as well as team preference. One thing to remember is that these new state functions don’t really give us anything that React couldn’t already do, but rather lets us use state in more situations, with a new concise syntax.

I personally really like the hooks syntax so I will try to use functional components as much as possible. However, I don’t plan on re-writing all of my class components, and there are some cases where classes may either be preferred or necessary.

Here is my current thought process on when to use each state method.

#### Class Component State

- Although there is pretty good coverage of lifecycle methods and additions class functionality across various hooks, there are still a few cases that can only be achieved via class components. The [FAQ section](https://reactjs.org/docs/hooks-faq.html#do-hooks-cover-all-use-cases-for-classes) in the documentation mentions a few of the cases, as well as a warning that some third-party components may not yet fully support hooks.
- If you are working on existing code that is written out in classes, don’t feel like you need to spend time refactoring something that isn’t broken. React has no plans to deprecate class components, so I plan to keep these existing components in the class form.
- If you just prefer class syntax, use classes!

#### Functional useState

- Whenever I have a state that changes in a simple and predictable way, `useState` will be my go-to starting point. Things such as on/off toggles, form state, simple page data are perfect candidates for `useState`.

#### Functional useReducer

- Whenever I have a state that changes more dynamically than on/off, `useReducer` is likely the tool for the job. Let’s say there as an array of items rendered to the page that needs to be interactive to the user. We may need to sort, filter, update, or delete this data, which will require us to go in and manipulate our array in different ways based on the operation. This would be a perfect opportunity to set up a reducer function and pass in the action and data as need.

Now take an afternoon, make a few functional components and give some of the hooks a shot! The best way to get a feel for these new state methods is to get your hands dirty and try them out in your own applications. Happy Coding!
