import axios from 'axios'

const groupProjectsEndpoint = group =>
  `https://gitlab.com/api/v4/groups/${group}/projects?per_page=100`

const contributionsEndpoint = (user, page) =>
  `https://gitlab.com/api/v4/users/${user}/events?page=${page}&per_page=100`

const userEndpoint = user => `https://gitlab.com/api/v4/users/?username=${user}`

/*
Takes a group name and returns all public projectID's belonging to that group
*/
const getGroupProjects = async groupName => {
  const res = await axios.get(groupProjectsEndpoint(groupName))
  return res.data.map(project => project.id)
}

/*
Gets all contributions from a user, then filters, out non-skillcamp contributions
Gitlab data is paginated, so it grabs up to 100 contributions at a time, then fetches the next pages
*/
const getContributions = async (username, projects) => {
  let contributions = []
  let promises = []

  if ((await username.indexOf('.')) > -1) {
    let uid = await axios.get(userEndpoint(username))
    username = uid.data[0].id
  }

  let res = await axios.get(contributionsEndpoint(username, 1))
  const pages = res.headers['x-total-pages']
  contributions = contributions.concat(res.data)

  if (pages > 1) {
    for (let i = 2; i <= pages; i++) {
      promises.push(
        Promise.resolve(axios.get(contributionsEndpoint(username, i)))
      )
    }
  }

  return Promise.all(promises).then(pages => {
    pages.forEach(page => (contributions = contributions.concat(page.data)))
    return contributions.filter(con => projects.includes(con.project_id))
  })
}

/*
Takes a username and group, returns an array of user events related
to that group.
 */
const getGroupContributions = async (username, group) => {
  const projects = await getGroupProjects(group)
  return await getContributions(username, projects)
}

/*
Takes a username and returns date of first contribution and the number of
contributions made to skillcamp projects. Checks and stores data in local storage.
 */
const skillcampContributions = async username => {
  const oneHour = 3600000

  // Checks local storage for value, before fetching data, value expires in 1 hour
  let firstContribution = localStorage.getItem(`${username}_first_contribution`)
  const storedContributions = localStorage.getItem(`${username}_contributions`)
  const expiration = localStorage.getItem(`${username}_contribution_expiration`)
  const elapsed = Date.now() - expiration

  if (firstContribution && storedContributions && elapsed < oneHour) {
    return { firstContribution, contributions: storedContributions }
  } else {
    const contributions = await getGroupContributions(username, 'skillcamp')

    if (!firstContribution) {
      firstContribution = contributions
        .map(con => new Date(con.created_at))
        .sort((a, b) => a - b)[0]
      localStorage.setItem(`${username}_first_contribution`, firstContribution)
    }

    localStorage.setItem(`${username}_contributions`, contributions.length)
    localStorage.setItem(`${username}_contribution_expiration`, Date.now())
    return { firstContribution, contributions: contributions.length }
  }
}

/*
Takes an array of contributions as parameter. getGroupContributions() would work
Returns an object of action types with totals (comments pushes etc..)
 */
const dataBreakdown = contributions => {
  return contributions.map(con => con.action_name).reduce((acc, curr) => {
    if (acc[`${curr}`]) {
      acc[`${curr}`]++
    } else {
      acc[`${curr}`] = 1
    }
    return acc
  }, {})
}

export { skillcampContributions, getGroupContributions, dataBreakdown }
