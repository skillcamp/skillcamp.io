import React from 'react'
import styled from 'styled-components'
import { graphql } from 'gatsby'
import FooterlessLayout from '../components/FooterlessLayout'
import distanceInWordsStrict from 'date-fns/distance_in_words_strict'
import { skillcampContributions } from '../../apis/gitlab'
import Sidebar from '../components/Community/Sidebar'
import About from '../components/Community/About'

const Container = styled.div`
  margin-left: 200px;

  @media screen and (min-width: 1300px) {
    margin-left: 300px;
  }

  @media screen and (max-width: 767px) {
    margin-left: initial;
  }
`

class CommunityMemberLayout extends React.Component {
  state = {
    contributions: undefined,
    skillcamp_age: undefined,
    mobile: true,
  }

  getContributions = () => {
    const { gitlab } = this.props.data.markdownRemark.frontmatter
    const skillcamp_age = localStorage.getItem(`${gitlab}_first_contribution`)

    if (skillcamp_age) {
      this.setState({ skillcamp_age })
    }

    skillcampContributions(gitlab).then(response => {
      this.setState({
        contributions: response.contributions,
        skillcamp_age: response.firstContribution,
      })
    })
  }

  componentDidMount() {
    const { gitlab } = this.props.data.markdownRemark.frontmatter
    const mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    )

    if (gitlab && !mobile) {
      this.setState({ mobile: false })
      this.getContributions()
    }
  }

  render() {
    const { location } = this.props
    const { contributions, mobile, skillcamp_age } = this.state
    const { markdownRemark: member } = this.props.data
    const { frontmatter, html } = member
    const { name, role, avatar, tagline } = frontmatter
    const socials = {
      facebook: frontmatter.facebook,
      github: frontmatter.github,
      gitlab: frontmatter.gitlab,
      linkedin: frontmatter.linkedin,
      soundcloud: frontmatter.soundcloud,
      twitter: frontmatter.twitter,
      website: frontmatter.website,
    }
    return (
      <FooterlessLayout location={location}>
        <Container>
          <Sidebar
            role={role}
            name={name}
            tagline={tagline}
            socials={socials}
          />
          <About
            content={html}
            fixed={avatar.childImageSharp.resolutions}
            skillcamp_age={
              skillcamp_age &&
              distanceInWordsStrict(this.state.skillcamp_age, Date.now(), {
                partialMethod: 'round',
              })
            }
            mobile={mobile}
            contributions={contributions}
          />
        </Container>
      </FooterlessLayout>
    )
  }
}

export const memberQuery = graphql`
  query CommunityMemberByPath($_path: String!) {
    markdownRemark(frontmatter: { path: { eq: $_path } }) {
      html
      frontmatter {
        name
        tagline
        role
        path
        facebook
        github
        gitlab
        linkedin
        soundcloud
        twitter
        website
        avatar {
          childImageSharp {
            resolutions(width: 200, height: 200, quality: 90) {
              ...GatsbyImageSharpResolutions
            }
          }
        }
      }
    }
  }
`

export default CommunityMemberLayout
