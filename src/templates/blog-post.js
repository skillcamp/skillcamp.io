import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import Layout from '../components/layout'

const Wrapper = styled.div`
  padding: 0 20px 30px;
  max-width: 900px;
  display: flex;
  flex-direction: column;
  margin: 0 auto;

  p,
  li {
    font-size: 19px;
  }

  img {
    box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
  }
`

const Cover = styled.div`
  width: 100vw;
  height: fit-content;
  max-height: 500px;
  margin-bottom: 50px;
  background: linear-gradient(to right, #444, #222);
  overflow: hidden;
`

const CoverImage = styled(Img)`
  max-width: 900px;

  margin: 0 auto;

  img {
    max-height: 500px !important;
    object-fit: scale-down !important;
  }
`

const Avatar = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 26.1px;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  p {
    margin: 0;
    font-size: 17px;
    font-weight: 600;
  }

  span {
    color: #888;
    font-size: 14px;
    font-weight: 400;
  }
`

const AvatarImage = styled(Img)`
  margin: 0 10px 0 0;
  height: 60px;
  width: 60px;
  border-radius: 50%;
`

class BlogPostLayout extends React.Component {
  render() {
    const { data, location } = this.props
    const { markdownRemark: post } = data
    const { frontmatter, html } = post
    const { title, date, coverImage, author, avatar } = frontmatter

    return (
      <Layout location={location}>
        <div style={{ overflowX: 'hidden' }}>
          <Helmet title={`${title}`} />
          <Cover>
            <CoverImage sizes={coverImage.childImageSharp.sizes} />
          </Cover>
          <Wrapper>
            <h1>{title}</h1>
            <Avatar>
              <AvatarImage fixed={avatar.childImageSharp.resolutions} />
              <div>
                <p>{author}</p>
                <span>{date}</span>
              </div>
            </Avatar>
            <div dangerouslySetInnerHTML={{ __html: html }} />
          </Wrapper>
        </div>
      </Layout>
    )
  }
}

export const pageQuery = graphql`
  query($_path: String!) {
    markdownRemark(frontmatter: { path: { eq: $_path } }) {
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        author
        avatar {
          childImageSharp {
            resolutions(width: 60, height: 60, quality: 80) {
              ...GatsbyImageSharpResolutions_withWebp_noBase64
            }
          }
        }
        path
        excerpt
        coverImage {
          childImageSharp {
            sizes(maxWidth: 900, quality: 80) {
              ...GatsbyImageSharpSizes_withWebp_noBase64
            }
          }
        }
      }
    }
  }
`

export default BlogPostLayout
