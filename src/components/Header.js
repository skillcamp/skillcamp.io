import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import logo from '../images/skillcamp_logo.svg'
import MenuIcon from './MenuIcon'

const HeaderWrapper = styled.div`
  width: 100%;
  position: fixed;
  top: 0;
  background: ${props =>
    props.atTop && props.isHome
      ? 'transparent'
      : 'linear-gradient(to right, #FDA253, #fbc15a)'};
  color: #1b1b1b;
  z-index: 100;
  transition: background 200ms;

  @media screen and (max-width: 600px) {
    position: relative;
    background: linear-gradient(to right, #fda253, #fbc15a);
  }
`

const Container = styled.div`
  height: ${props => (props.active && props.mobile ? '400px' : '60px')};
  display: grid;
  grid-template-columns: 1fr;
  grid-template-areas: 'logo  navigation';
  justify-content: space-between;
  align-items: center;
  transition: all 200ms ease;
  max-width: 1440px;
  margin: 0 auto;

  @media screen and (max-width: 600px) {
    padding: 15px 0;
    overflow: hidden;
    align-items: flex-start;
    grid-template-columns: repeat(2, 1fr);
    grid-template-areas:
      'logo       icon'
      'navigation navigation';
  }
`

const LogoWrapper = styled(Link)`
  grid-area: logo;
  margin-left: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  justify-self: flex-start;
  color: white;
  text-decoration: none;
`

const Logo = styled.img`
  height: 30px;
  margin: 0;
  transform: ${props =>
    props.atTop ? 'scale(1.4) translateX(20px)' : 'scale(1)'};
  transition: transform 200ms ease;

  @media screen and (max-width: 750px) {
    padding-right: 16px;
    transform: scale(1);
  }
  @media screen and (max-width: 600px) {
    transform: scale(1.2) translateX(10px);
  }
`

const NavContainer = styled.div`
  grid-area: navigation;
  display: flex;
  align-items: stretch;
  justify-content: center;
  align-self: stretch;
  font-size: 17px;
  font-weight: lighter;
  text-align: right;
  animation: ${props => (props.mobile ? 'grow 200ms ease' : 'none')};

  a:link,
  a:visited,
  a:active {
    padding: 8px 20px;
    text-decoration: none;
    @media screen and (max-width: 840px) {
      padding: 8px 14px;
    }
    @media screen and (max-width: 750px) {
      padding: 8px 10px;
    }
  }

  a:hover {
    text-decoration: none;
  }

  .button {
    color: white;
  }
  @media screen and (max-width: 1440px) {
    padding-right: 16px;
  }
  @media screen and (max-width: 750px) {
    margin-right: 8px;
  }
  @media screen and (max-width: 600px) {
    padding-right: 0;
    height: 100%;
    text-align: center;
    margin: 0;
    flex-direction: column;
    justify-content: space-between;
  }

  @keyframes grow {
    from {
      transform: scaleY(0);
      opacity: 0;
    }
    to {
      transform: scaleY(1);
      opacity: 1;
    }
  }
`

const Links = styled.div`
  display: flex;
  align-items: center;
  margin-right: 12px;

  a {
    display: flex;
    align-items: center;
    height: 100%;
    color: rgba(0, 0, 0, 0.8);
    font-weight: 700;

    &:hover {
      color: #000;
      background: rgb(255, 206, 114);
    }
  }

  @media screen and (max-width: 784px) {
    a {
      font-size: 15px;
    }
  }

  @media screen and (max-width: 600px) {
    min-height: 200px;
    flex-direction: column;
    margin: 0 auto;
    justify-content: space-around;

    a {
      width: 80vw;
      height: 100%;
      border-bottom: 1px dotted #1b1b1b;
      display: flex;
      align-items: center;
      justify-content: center;
      font-size: 17px;
    }
  }
`

const Button = styled(Link)`
  width: 184px;
  min-width: 152px;
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #22a477;
  color: white;
  border: none;
  border-radius: 3px;
  align-self: center;
  margin-left: 8px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  font-size: 17px;
  transition: all 200ms ease;

  &:hover {
    background: #23c08f;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    transform: translateY(-3px);
  }

  @media screen and (max-width: 750px) {
    font-size: 15px;
    width: 140px;
  }

  @media screen and (max-width: 600px) {
    width: 200px;
    height: 50px;
    justify-self: center;
    margin: 10px 0;
  }
`

class Header extends React.Component {
  state = {
    scrollHeightZero: true,
    mobile: false,
    menuActive: false,
    resizeListener: null,
    scrollListener: null,
  }

  toggleMenu = () => {
    if (this.state.mobile) {
      this.setState(prevState => ({ menuActive: !prevState.menuActive }))
    }
  }

  componentDidMount() {
    if (typeof window !== undefined) {
      this.setState({ mobile: window.innerWidth <= 600 })
      this.setState({ scrollHeightZero: window.scrollY <= 60 })

      const resize = () => {
        if (!this.state.mobile && window.innerWidth <= 600) {
          this.setState({ mobile: true })
        } else if (this.state.mobile && window.innerWidth >= 600) {
          this.setState({ mobile: false })
        }
      }
      const resizeListener = window.addEventListener('resize', resize)
      this.setState({ resizeListener })

      const scroll = () => {
        if (!this.state.scrollHeightZero && window.scrollY <= 60) {
          this.setState({ scrollHeightZero: true })
        } else if (this.state.scrollHeightZero && window.scrollY >= 60) {
          this.setState({ scrollHeightZero: false })
        }
      }
      const scrollListener = window.addEventListener('scroll', scroll)
      this.setState({ scrollListener })
    }
  }

  componentWillUnmount() {
    if (typeof window !== undefined) {
      const { resizeListener, scrollListener } = this.state

      resizeListener && window.removeEventListener('resize', resizeListener)
      this.setState({ resizeListener: null })

      scrollListener && window.removeEventListener('scroll', scrollListener)
      this.setState({ scrollListener: null })
    }
  }

  componentDidUpdate(prevProps) {
    // close mobile menu if open and route pathname has changed
    if (
      this.state.menuActive === true &&
      this.props.location.pathname !== prevProps.location.pathname
    ) {
      this.setState({ menuActive: false })
    }
  }

  render() {
    const { scrollHeightZero, mobile, menuActive } = this.state

    // check to see if home, only have transparent header at top on home.
    const isHome = this.props.location.pathname === '/'
    return (
      <HeaderWrapper
        data-testid="header-wrapper"
        atTop={scrollHeightZero}
        isHome={isHome}
      >
        <Container active={menuActive} mobile={mobile}>
          <LogoWrapper to="/">
            <Logo
              src={logo}
              alt="skillcamp-logo"
              atTop={scrollHeightZero}
              data-testid="logo"
            />
          </LogoWrapper>
          {(!mobile || menuActive) && (
            <NavContainer data-testid="nav-container" mobile={mobile}>
              <Links>
                <Link to="/blog/what-is-skillcamp">about</Link>
                <a
                  href="https://gitlab.com/skillcamp"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  projects
                </a>
                <Link to="/blog">blog</Link>
                <Link to="/community">community</Link>
              </Links>
              <Button to="/join" className="button">
                Join Skillcamp
              </Button>
            </NavContainer>
          )}
          {mobile && (
            <MenuIcon
              data-testid="menu-icon"
              toggleMenu={this.toggleMenu}
              menuActive={menuActive}
            />
          )}
        </Container>
      </HeaderWrapper>
    )
  }
}

export default Header
