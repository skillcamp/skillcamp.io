import React from 'react'
import styled from 'styled-components'
import { SocialAccounts } from './SocialAccounts'

const Container = styled.div`
  background-color: #2a2a2a;
  padding: 15px;
  width: 300px;
  overflow: hidden;
  position: fixed;
  top: 60px;
  bottom: 0;
  left: 0;

  @media screen and (min-width: 1300px) {
    width: 400px;
  }

  @media screen and (max-width: 767px) {
    position: relative;
    top: 0;
    width: 100%;
    padding-bottom: 130px;
  }
`

const MemberInfo = styled.div`
  position: relative;
  z-index: 2;

  @media screen and (max-width: 767px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`

const Role = styled.h2`
  color: #f2f2f2;
  font-size: 21px;
  font-weight: normal;
  margin: 0 0 30px;
`

const Name = styled.h2`
  max-width: 170px;
  font-size: 28px;
  color: #ffffff;
  margin: 0 0 30px;

  @media screen and (min-width: 1300px) {
    max-width: 265px;
    font-size: 44px;
  }

  @media screen and (max-width: 767px) {
    max-width: 100%;
    text-align: center;
    font-size: 38px;
  }
`

const Tagline = styled.h2`
  color: #8e8e8e;
  font-size: 18px;
  font-weight: normal;
  margin: 0 0 30px;
  width: 50%;

  @media screen and (max-width: 767px) {
    text-align: center;
  }
`

const RectContainer = styled.div`
  width: 1200px;
  margin-left: -400px;
  margin-top: 110px;
  transform: rotate(-30deg);
  position: absolute;
  bottom: -80px;

  @media screen and (max-width: 767px) {
    display: none;
  }
`

const Rect = styled.div`
  width: 100%;
  height: 125px;
  background-color: ${props => props.color};
`

const Sidebar = ({ name, role, tagline, socials }) => {
  const fullname = name.split(' ')
  return (
    <Container>
      <MemberInfo>
        <Role>{role}</Role>
        <Name>
          {fullname[0]} {fullname[1]}
        </Name>
        <Tagline>{tagline}</Tagline>
        <SocialAccounts {...socials} />
      </MemberInfo>
      <RectContainer>
        <Rect color="#03C9F1" />
        <Rect color="#FFC14A" />
        <Rect color="#F45247" />
      </RectContainer>
    </Container>
  )
}

export default Sidebar
