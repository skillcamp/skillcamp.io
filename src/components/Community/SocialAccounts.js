import React from 'react'
import styled from 'styled-components'

const Links = styled.div`
  width: 50%;
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 24px;
  align-items: center;
  align-self: center;

  @media screen and (max-width: 767px) {
    width: 100%;
    justify-content: center;
  }

  img {
    margin: 10px;
    width: 25px;
    height: 25px;
    transition: all 200ms ease;
  }

  img:hover {
    filter: brightness(1.2);
    transform: translateY(-3px);
  }
`
let resolveURL = (key, value) => {
  switch (key) {
    case 'facebook':
      return `https://facebook.com/${value}`
    case 'github':
      return `https://github.com/${value}`
    case 'gitlab':
      return `https://gitlab.com/${value}`
    case 'linkedin':
      return `https://www.linkedin.com/in/${value}`
    case 'slack':
      return `https://skillcamp-io.slack.com/app_redirect?channel=@${value}`
    case 'soundcloud':
      return `https://www.soundcloud.com/${value}`
    case 'twitter':
      return `https://www.twitter.com/${value}`
    case 'website':
      return value
    default:
      return ''
  }
}

class SocialAccounts extends React.PureComponent {
  render() {
    return (
      <Links>
        {Object.keys(this.props).map((key, index) => {
          let value = this.props[key]
          let linkNotValid =
            value === null || value === undefined || value === ''
          if (!linkNotValid) {
            let url = resolveURL(key, value)
            let socialMediaLogo = require(`../../images/${key}.png`)
            return (
              <a
                key={index}
                href={url}
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src={socialMediaLogo} alt={key + '.png'} />
              </a>
            )
          }
          return null
        })}
      </Links>
    )
  }
}

export { SocialAccounts }
