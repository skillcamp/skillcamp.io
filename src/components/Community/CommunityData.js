import React from 'react'
import styled from 'styled-components'
import Loading from '../Loading'

const Contributions = styled.div`
  height: 150px;
  width: 150px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: 5px solid white;
  background: linear-gradient(to bottom right, #444, #111);
  color: #fff;
  border-radius: 50%;
  margin-bottom: 45px;

  @media screen and (max-width: 767px) {
    display: none;
  }

  p {
    margin: 0;
    letter-spacing: 1px;
  }

  .text {
    width: ${props => (props.text === 'SkillCamp Age' ? '50%' : 'auto')};
    margin-bottom: 13px;
    font-size: 14px;
    font-weight: lighter;
    line-height: 16px;
    text-align: center;
  }

  .number {
    font-size: 20px;
    font-weight: bold;
  }
`

const LoadingWrapper = styled.div`
  height: 26px;
  padding-top: 5px;
  display: flex;
  align-items: center;
`

export default ({ text, data }) => (
  <Contributions loading={data} text={text}>
    <p className="text">{text}</p>
    {data ? (
      <p className="number">{data}</p>
    ) : (
      <LoadingWrapper>
        <Loading />
      </LoadingWrapper>
    )}
  </Contributions>
)
