import React from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'

const Avatar = props => {
  return (
    <div>
      <Image fixed={props.fixed} />
    </div>
  )
}

const Image = styled(Img)`
  border-radius: 50%;
  border: 5px solid white;
  margin-bottom: 45px;

  @media screen and (max-width: 767px) {
    margin-bottom: 20px;
  }
`

export default Avatar
