import React from 'react'
import styled from 'styled-components'
import Avatar from './Avatar'
import CommunityData from './CommunityData'

const Container = styled.div`
  background-color: #ffffff;
  padding-bottom: 0;
  display: flex;
  margin-top: 80px;

  @media screen and (max-width: 767px) {
    flex-direction: column;
    margin-top: -40px;
  }
`

const CommunityContent = styled.div`
  max-width: 1440px;
  margin: 20px 70px;
`

const AvatarDataContainer = styled.div`
  height: fit-content;
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 2;
  position: sticky;
  top: 80px;

  @media screen and (min-width: 1300px) {
    left: 300px;
  }

  @media screen and (max-width: 767px) {
    margin-top: -70px;
    position: initial;
  }
`

const About = ({ content, fixed, contributions, skillcamp_age, mobile }) => {
  return (
    <Container>
      <AvatarDataContainer>
        <Avatar fixed={fixed} />
        {!mobile && (
          <>
            <CommunityData data={contributions} text="Contributions" />
            <CommunityData data={skillcamp_age} text="Skill Camp Age" />
          </>
        )}
      </AvatarDataContainer>
      <CommunityContent dangerouslySetInnerHTML={{ __html: content }} />
    </Container>
  )
}

export default About
