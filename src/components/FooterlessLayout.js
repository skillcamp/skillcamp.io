import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Header from './Header'
import '../styles/index.css'
import favicon_32 from '../favicon/favicon-32x32.png'
import favicon_16 from '../favicon/favicon-16x16.png'
import apple_touch from '../favicon/apple-touch-icon.png'
import safari_pinned from '../favicon/safari-pinned-tab.svg'
import styled from 'styled-components'

const Wrapper = styled.div`
  margin-top: 60px;

  @media screen and (max-width: 600px) {
    margin-top: 0;
  }
`

const Layout = ({ children, location }) => (
  <div>
    <Helmet title="SkillCamp">
      {/*Favicon Stuff*/}
      <html lang="en" />
      <meta
        name="description"
        content="An open source community for developers to improve their skills through collaboration."
      />
      <meta
        name="keywords"
        content="skillcamp, developer, mentor, learning, open source, community"
      />
      <link rel="apple-touch-icon" sizes="180x180" href={apple_touch} />
      <link rel="icon" type="image/png" sizes="32x32" href={favicon_32} />
      <link rel="icon" type="image/png" sizes="16x16" href={favicon_16} />
      <link rel="mask-icon" href={safari_pinned} color="#fbc15a" />
      <meta name="msapplication-TileColor" content="#fbc15a" />
      <meta name="theme-color" content="#fbc15a" />
    </Helmet>
    <Header location={location} />
    <Wrapper>{children}</Wrapper>
  </div>
)

Layout.propTypes = {
  children: PropTypes.object,
}

export default Layout
