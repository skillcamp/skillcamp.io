import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import CardList from './CardList'

const AboutWrapper = styled.div`
  margin: 0 auto;
  max-width: 1440px;
`
const TextWrapper = styled.div`
  max-width: 90vw;
  margin: 120px auto;
  line-height: 30px;
  font-size: 18px;

  @media (min-width: 1024px) {
    max-width: 960px;
  }
`

const Button = styled(Link)`
  width: 180px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 120px auto 80px auto;
  background: #20a375;
  color: white;
  border-radius: 3px;
  text-decoration: none;
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
  transition: all 200ms ease;

  &:hover {
    background: #23c08f;
    transform: translateY(-3px);
  }

  @media (max-width: 850px) {
    margin: 0 auto 80px auto;
  }
`

const ButtonWrapper = styled.div`
  display: block;
  text-align: center;
`

export default () => (
  <AboutWrapper>
    <TextWrapper>
      <p>
        <strong>
          Learning to code is hard. Luckily, you don't have to do it alone!
        </strong>
        <br />
        Skillcamp is a group of like-minded developers, who aim to improve their
        own skills through collaboration, as well as help others along the way.
      </p>
      <p>
        Whether you're a junior developer looking for experience, mid-level
        developer looking to gain some project management skills, or a seasoned
        CTO, Skillcamp is a rewarding experience for everyone!
      </p>
    </TextWrapper>
    <CardList />
    <ButtonWrapper>
      <Button to="/join">Join Skillcamp</Button>
    </ButtonWrapper>
  </AboutWrapper>
)
