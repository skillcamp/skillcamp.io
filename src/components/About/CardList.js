import React from 'react'
import styled from 'styled-components'
import checkmark from '../../images/check_mark.svg'
import developer from '../../images/developer.svg'
import mentor from '../../images/mentor.svg'

const CardListWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;

  @media (max-width: 850px) {
    flex-direction: column;
    align-items: center;
  }
`

const CardWrapper = styled.div`
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.2);
  height: 450px;
  border-radius: 5px;
  min-width: 400px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 20px;

  @media (max-width: 850px) {
    margin-bottom: 100px;
  }

  @media (max-width: 480px) {
    min-width: 357px;
  }
`

const CardTitle = styled.h3`
  border-top: 2px solid #333;
  border-radius: 5px 5px 0 0;
  background: #333;
  color: #fff;
  text-align: center;
  line-height: 2em;
  margin: 0;
`

const CardThumbnail = styled.img`
  width: 150px;
  height: 150px;
  margin: 20px auto;
  display: block;
`

const DescriptionWrapper = styled.div`
  width: 90%;
  margin: 0 auto 10px auto;
  display: flex;
  align-items: center;
  justify-self: flex-end;
  align-self: center;
`

const CheckMark = styled.img`
  height: 20px;
  width: 20px;
  margin: 0 8px 0 0;
`

const CardDescription = styled.p`
  font-weight: lighter;
  margin: 0;
`

const Card = ({ role, src, alt, highlights }) => {
  return (
    <CardWrapper>
      <CardTitle>{role}</CardTitle>
      <CardThumbnail src={src} alt={alt} />
      <div>
        {highlights.map((highlight, i) => (
          <DescriptionWrapper key={i}>
            <CheckMark alt="check_mark" src={checkmark} />
            <CardDescription>{highlight}</CardDescription>
          </DescriptionWrapper>
        ))}
      </div>
    </CardWrapper>
  )
}

class CardList extends React.Component {
  state = {
    cards: [
      {
        role: 'Developers',
        alt: 'Skillcamp Developer',
        src: developer,
        highlights: [
          'Learn to effectively collabrate with a team',
          'Join a growing community of developers',
          'Receive feedback to improve your code',
          'Discover and learn modern tech',
        ],
      },
      {
        role: 'Mentors',
        alt: 'Skillcamp Mentor',
        src: mentor,
        highlights: [
          'Inspire the next generation of developers',
          'Help shape a supportive community',
          'Develop strong leadership skills',
          'Learn through teaching',
        ],
      },
    ],
  }

  renderCards = () => {
    return this.state.cards.map((card, i) => (
      <Card
        key={i}
        role={card.role}
        alt={card.alt}
        src={card.src}
        highlights={card.highlights}
      />
    ))
  }

  render() {
    return <CardListWrapper>{this.renderCards()}</CardListWrapper>
  }
}

export default CardList
