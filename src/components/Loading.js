import React from 'react'
import styled from 'styled-components'

const Loader = styled.div`
  margin: ${props => props.margin};
  width: 10px;
  height: 5px;
  background: #fff;
  color: #fff;
  position: relative;
  animation: load1 1s infinite ease-in-out;
  animation-delay: -0.16s;

  &:before,
  &:after {
    width: 10px;
    height: 5px;
    background: #fff;
    position: absolute;
    top: 0;
    content: '';
    animation: load1 1s infinite ease-in-out;
  }

  &:before {
    left: -15px;
    animation-delay: -0.32s;
  }

  &:after {
    left: 15px;
  }

  @keyframes load1 {
    0%,
    80%,
    100% {
      box-shadow: 0 0;
      height: 5px;
    }
    40% {
      box-shadow: 0 -9px;
      height: 10px;
    }
  }
`

export default ({ margin }) => <Loader margin={margin} />
