import React from 'react'
import { render, fireEvent } from 'react-testing-library'
import Header from './Header'

const location = { pathname: '/' }

describe('Header test suite', () => {
  test('Renders header', () => {
    const { container } = render(<Header location={location} />)
    expect(container.firstChild).toMatchSnapshot()
  })

  test('Nav links route to correct location', () => {
    const { getByText } = render(<Header location={location} />)
    const about = getByText('about')
    const projects = getByText('projects')
    const blog = getByText('blog')
    const community = getByText('community')
    const join = getByText('Join Skillcamp')

    expect(about).toHaveAttribute('href', '/blog/what-is-skillcamp')
    expect(projects).toHaveAttribute('href', 'https://gitlab.com/skillcamp')
    expect(blog).toHaveAttribute('href', '/blog')
    expect(community).toHaveAttribute('href', '/community')
    expect(join).toHaveAttribute('href', '/join')
  })

  test('Styles render correctly at top of page', () => {
    global.scrollY = 60
    const { getByTestId, rerender } = render(<Header location={location} />)
    const logo = getByTestId('logo')
    const wrapper = getByTestId('header-wrapper')

    expect(logo).toHaveStyleRule('transform', 'scale(1.4) translateX(20px)')
    expect(wrapper).toHaveStyleRule('background', 'transparent')

    // Any path that is not "/"
    rerender(<Header location={{ pathName: '/notHome' }} />)
    expect(wrapper).toHaveStyleRule(
      'background',
      'linear-gradient(to right,#FDA253,#fbc15a)'
    )
  })

  test('Styles render after scrolling down page', () => {
    global.scrollY = 61
    const { getByTestId } = render(<Header location={location} />)
    const logo = getByTestId('logo')
    const wrapper = getByTestId('header-wrapper')

    expect(logo).toHaveStyleRule('transform', 'scale(1)')
    expect(wrapper).toHaveStyleRule(
      'background',
      'linear-gradient(to right,#FDA253,#fbc15a)'
    )
  })

  test('Mobile menu renders under 600px', () => {
    global.innerWidth = 600
    const { getByTestId } = render(<Header location={location} />)
    const menu = getByTestId('menu-button')
    expect(menu)
  })

  test('Mobile menu opens and closes correctly', () => {
    global.innerWidth = 600
    const { rerender, queryByTestId, getByTestId } = render(
      <Header location={location} />
    )

    // Menu Closed by default
    expect(queryByTestId('nav-container')).toBeNull()

    // Open menu
    fireEvent.click(getByTestId('menu-button'))
    expect(queryByTestId('nav-container')).not.toBeNull()

    // Menu closes on path change
    rerender(<Header location={{ pathname: '/somethingNew' }} />)
    expect(queryByTestId('nav-container')).toBeNull()
  })
})
