import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Img from 'gatsby-image'

const Wrapper = styled(Link)`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-decoration: none;
  cursor: pointer;
  transition: all 200ms ease-out;

  img {
    transition: all 200ms ease-out;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
  }

  &:hover {
    transform: translateY(-5px);
    text-decoration: underline #555;

    img {
      box-shadow: 0 9px 10px rgba(0, 0, 0, 0.3);
    }
  }
`

const Image = styled(Img)`
  height: 200px;
  width: 200px;
  border-radius: 50%;
  align-self: center;

  img {
    // over-rides inline styling from gatsby-plugin-image
    height: 200px;
    width: 200px;
    object-fit: scale-down !important;
    object-position: center !important;
  }

  @media screen and (max-width: 900px) and (min-width: 700px) {
    height: 150px;
    width: 150px;

    img {
      // over-rides inline styling from gatsby-plugin-image
      height: 150px;
    }
  }
`

const Content = styled.div`
  max-width: 250px;
  margin-top: 20px;

  h3 {
    color: #333;
    margin: 0;
    font-size: 18px;
    line-height: 22px;
  }

  p {
    width: 100%;
    color: #777;
    margin: 10px 0;
    font-size: 15px;
  }

  @media screen and (max-width: 900px) and (min-width: 700px) {
    max-width: 200px;
  }
`

const BlogPost = ({ frontmatter }) => {
  const { title, author, thumbnail, path } = frontmatter
  return (
    <Wrapper to={`/blog/${path}`}>
      <Image
        sizes={thumbnail.childImageSharp.sizes}
        backgroundColor="#eeeeee"
      />
      <Content>
        <h3 name="title">{title}</h3>
        <p name="author">{author}</p>
      </Content>
    </Wrapper>
  )
}

export default BlogPost
