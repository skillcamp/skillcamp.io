import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { WithContext as ReactTags } from 'react-tag-input'

const Wrapper = styled.div`
  margin-bottom: 25px;
  width: 100%;
  max-width: 900px;
  font-family: 'Nunito Sans', Avenir, Helvetica, sans-serif;
  font-weight: bold;

  .ReactTags__tagInput {
    position: relative;
    width: fit-content;
    border-radius: 2px;
    display: flex;
    justify-content: center;
    margin-right: 20px;

    @media screen and (max-width: 600px) {
      width: 90%;
      margin-right: 0;
    }
  }

  .ReactTags__selected {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    margin-bottom: 15px;

    @media screen and (max-width: 600px) {
      flex-direction: column;
      justify-content: flex-start;
      align-items: center;
    }
  }

  .ReactTags__selected .ReactTags__tag {
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 35px;
    padding-left: 10px;
    padding-right: 10px;
    margin-right: 5px;
    background-color: #eee;
    border-radius: 100px;
    transition: 100ms ease;

    &:hover {
      background-color: #d2d2d2;
    }

    ::before {
      content: '#';
    }

    @media screen and (max-width: 600px) {
      margin: 0 0 10px 0;
    }
  }

  .ReactTags__selected .ReactTags__remove {
    margin-left: 5px;
    cursor: pointer;
    color: #f24d47;
  }

  .ReactTags__tagInputField {
    outline: none;
    height: 40px;
    width: 100%;
    border: 3px rgba(251, 192, 90, 0.41) solid;
    border-radius: 5px;
    transition: all 100ms ease-out;
    padding-left: 15px;
    cursor: pointer;

    &:focus:hover {
      cursor: text;
    }

    &:focus {
      border: 3px #fbc657 solid;
      border-radius: 30px;
    }
  }

  .ReactTags__suggestions ul .ReactTags__activeSuggestion {
    background: #eee;
    cursor: pointer;
  }

  .ReactTags__suggestions {
    position: absolute;
    top: 45px;
    z-index: 10;
    background-color: white;
    border-radius: 10px;
    width: 300px;
    box-shadow: 0 13px 27px -5px rgba(50, 50, 93, 0.25),
      0 8px 16px -8px rgba(0, 0, 0, 0.3);

    ul {
      list-style-type: none;
      font-family: 'Nunito Sans', Avenir, Helvetica, sans-serif;
      font-weight: bold;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
      align-items: center;
      margin: 0;
      li {
        width: 100%;
        padding: 10px 20px;
        margin: 0;
        mark {
          text-decoration: underline;
          background: none;
          font-weight: 600;
        }
      }
    }
  }
`

const KeyCodes = {
  comma: 188,
  enter: 13,
}

const delimiters = [KeyCodes.comma, KeyCodes.enter]

class SearchBar extends React.Component {
  render() {
    const { addTag, removeTag, handleDrag, tags, suggestions } = this.props
    return (
      <Wrapper>
        <ReactTags
          tags={tags}
          suggestions={suggestions}
          handleDelete={removeTag}
          handleAddition={addTag}
          handleDrag={handleDrag}
          delimiters={delimiters}
          autofocus={false}
          placeholder="Search tags..."
        />
      </Wrapper>
    )
  }
}

SearchBar.propTypes = {
  addTag: PropTypes.func.isRequired,
  removeTag: PropTypes.func.isRequired,
  handleDrag: PropTypes.func.isRequired,
  tags: PropTypes.array.isRequired,
  suggestions: PropTypes.array.isRequired,
}

export default SearchBar
