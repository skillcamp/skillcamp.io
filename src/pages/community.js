import React from 'react'
import styled from 'styled-components'
import { Link, graphql } from 'gatsby'
import Img from 'gatsby-image'
import Layout from '../components/layout'

const Div = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: 1440px;
  margin: 100px auto 0 auto;
`

const OurCommunity = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0px 20px;
  text-align: center;
`

const MemberWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  margin: 50px;
  max-width: 1440px;
`

const Members = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`

const Member = styled(Link)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  width: 150px;
  margin: auto 30px 40px 30px;
  transition: transform 200ms ease, opacity 200ms;
  text-decoration: inherit;
  color: inherit;
  height: 300px;

  &:hover {
    cursor: pointer;
    opacity: 0.5;
    transform: translateY(-4px);
  }
`

const Avatar = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 15px;

  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`

const AvatarImage = styled(Img)`
  height: 60px;
  width: 60px;
  border-radius: 50%;
`

const Name = styled.div`
  text-align: center;
  font-weight: 600;
`

const Tagline = styled.div`
  text-align: center;
  font-size: 14px;
  color: #777;
`

class CommunityPage extends React.Component {
  render() {
    const { data, location } = this.props
    const { edges: developers } = data.getDevelopers
    const { edges: mentors } = data.getMentors

    const generateMembers = memberType => {
      return memberType.map(({ node: member }) => {
        return (
          <Member
            key={member.frontmatter.path}
            to={`/community/${member.frontmatter.path}`}
          >
            <Avatar>
              <AvatarImage
                fixed={member.frontmatter.avatar.childImageSharp.resolutions}
              />
            </Avatar>
            <Name>{member.frontmatter.name}</Name>
            <Tagline>{member.frontmatter.tagline}</Tagline>
          </Member>
        )
      })
    }

    return (
      <Layout location={location}>
        <Div>
          <OurCommunity>
            <h1> Our Community</h1>
            <p>
              These are the people that make Skillcamp possible.
              <br />
              Everybody is invited to join, contribute and become a part of our
              growing community.
            </p>
          </OurCommunity>
          <MemberWrapper>
            <h2>Mentors</h2>
            <Members>{generateMembers(mentors)}</Members>
            <h2>Developers</h2>
            <Members>{generateMembers(developers)}</Members>
          </MemberWrapper>
        </Div>
      </Layout>
    )
  }
}

export const query = graphql`
  {
    getMentors: allMarkdownRemark(
      filter: {
        fileAbsolutePath: { regex: "//community/.*/" }
        frontmatter: { role: { eq: "mentor" } }
      }
    ) {
      edges {
        node {
          id
          frontmatter {
            name
            tagline
            role
            slack
            path
            avatar {
              childImageSharp {
                resolutions(width: 150, height: 150, quality: 80) {
                  ...GatsbyImageSharpResolutions
                }
              }
            }
          }
        }
      }
    }
    getDevelopers: allMarkdownRemark(
      filter: {
        fileAbsolutePath: { regex: "//community/.*/" }
        frontmatter: { role: { eq: "developer" } }
      }
    ) {
      edges {
        node {
          id
          frontmatter {
            name
            tagline
            role
            path
            avatar {
              childImageSharp {
                resolutions(width: 150, height: 150, quality: 80) {
                  ...GatsbyImageSharpResolutions
                }
              }
            }
          }
        }
      }
    }
  }
`

export default CommunityPage
