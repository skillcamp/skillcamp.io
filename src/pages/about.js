import React from 'react'
import Layout from '../components/layout'

const AboutPage = props => (
  <Layout location={props.location}>
    <div>
      <h1>About</h1>
    </div>
  </Layout>
)

export default AboutPage
