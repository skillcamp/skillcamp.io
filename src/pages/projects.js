import React from 'react'
import Layout from '../components/layout'

const ProjectsPage = props => (
  <Layout location={props.location}>
    <div>
      <h1>Projects</h1>
    </div>
  </Layout>
)

export default ProjectsPage
