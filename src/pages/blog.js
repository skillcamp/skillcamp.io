import React, { Fragment } from 'react'
import { Link, graphql } from 'gatsby'
import styled from 'styled-components'
import Img from 'gatsby-image'
import Layout from '../components/layout'
import SearchBar from '../components/BlogSection/SearchBar'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 100px 20px;
  max-width: 1440px;

  @media screen and (min-width: 1440px) {
    margin: 100px auto;
  }
`
const Line = styled.hr`
  margin: 5px 0;
`

const Card = styled(Link)`
  min-height: 300px;
  width: 90vw;
  max-width: 900px;
  align-self: center;
  justify-self: center;
  display: flex;
  flex-direction: ${props => (props.reverse ? 'row-reverse' : 'row')};
  align-items: center;
  justify-content: flex-start;
  border-radius: 5px;
  padding: 10px 20px;
  text-decoration: none;
  color: #222;
  transition: transform 200ms ease;

  p {
    color: #777;
  }

  &:hover {
    background-color: #f8f8f8;
    transform: translateY(-4px);
  }

  @media screen and (max-width: 700px) {
    flex-direction: column;
  }
`

const Image = styled(Img)`
  height: 250px;
  width: 250px;
  min-width: 250px;
  margin: 0;
  border-radius: 5px;
  object-fit: cover;
  object-position: top left;

  @media screen and (max-width: 700px) {
    margin: 20px 0;
  }
`

const CardDetails = styled.div`
  height: 100%;
  width: 100%;
  margin: 20px 50px;
  display: flex;
  flex-direction: column;
  text-align: ${props => (props.reverse ? 'left' : 'right')};
  justify-content: space-around;

  span {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  @media screen and (max-width: 700px) {
    text-align: center;
  }
`

const Avatar = styled.div`
  display: flex;
  flex-direction: ${props => (props.reverse ? 'row' : 'row-reverse')};
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 26px;

  div {
    padding: 0 !important;
    height: 40px !important;
  }

  p {
    font-size: 16px;
    font-weight: 400;
    margin: 0;
  }
`

const AvatarImage = styled(Img)`
  margin: ${props => (props.reverse ? '0 10px 0 0' : '0 0 0 10px')};
  height: 40px;
  width: 40px;
  border-radius: 50%;

  img {
    margin: 0;
    height: 40px;
    width: 40px;
  }
`

const TagsList = styled.ul`
  display: flex;
  flex-direction: ${props => (props.reverse ? 'row' : 'row-reverse')};
  justify-content: flex-start;
  align-items: center;
  list-style-type: none;
  margin: 0;
  color: #777;
`

const Tag = styled.li`
  ::before {
    content: '#';
  }
  font-weight: bold;
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 0;
`

class BlogPage extends React.Component {
  state = {
    tags: [],
    suggestions: [],
  }

  componentDidMount() {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark
    this.extractTagsFromMarkdownToState(posts)
  }

  extractTagsFromMarkdownToState = posts => {
    let suggestions = []
    posts.forEach(({ node: { frontmatter: { tags } } }) => {
      tags.forEach(tag => {
        let double = false
        suggestions.forEach(obj => {
          if (obj.id === tag) double = true
        })
        if (!double) suggestions = [...suggestions, { id: tag, text: tag }]
      })
    })
    this.setState({ suggestions })
  }

  addTag = tag => {
    this.setState(state => ({
      tags: [...state.tags, tag],
    }))
  }

  removeTag = tag => {
    const { tags } = this.state
    this.setState({
      tags: tags.filter((e, i) => i !== tag),
    })
  }

  handleDrag = (tag, currPos, newPos) => {
    const tags = [...this.state.tags]
    const newTags = tags.slice()
    newTags.splice(currPos, 1)
    newTags.splice(newPos, 0, tag)
    this.setState({ tags: newTags })
  }

  render() {
    const { tags, suggestions } = this.state
    const { data, location } = this.props
    const { edges: posts } = data.allMarkdownRemark

    const generateBlogPosts = () => {
      let orderedPosts = []
      if (tags.length > 0) {
        tags.forEach(tag => {
          posts.forEach(post => {
            const {
              node: {
                frontmatter: { tags },
              },
            } = post
            tags.forEach(tagPost => {
              if (tag.id === tagPost) {
                let double = false
                orderedPosts.forEach(orderedPost => {
                  if (orderedPost.node.id === post.node.id) double = true
                })
                if (!double) orderedPosts.push(post)
              }
            })
          })
        })
      } else {
        orderedPosts = [...posts]
      }

      if (orderedPosts.length === 0) {
        return <div>No Posts Found</div>
      } else {
        return orderedPosts
          .map(({ node: post }, i) => {
            const { frontmatter } = post
            const reverse = i % 2 === 0 ? 'true' : undefined
            return (
              <div key={frontmatter.path}>
                <Card
                  reverse={reverse}
                  key={frontmatter.path}
                  to={`/blog/${frontmatter.path}`}
                >
                  <Image
                    fixed={frontmatter.thumbnail.childImageSharp.resolutions}
                  />
                  <CardDetails reverse={reverse}>
                    <h2>{frontmatter.title}</h2>
                    <Avatar reverse={reverse}>
                      <AvatarImage
                        fixed={frontmatter.avatar.childImageSharp.resolutions}
                        reverse={reverse}
                      />
                      <p>{frontmatter.author}</p>
                    </Avatar>
                    <p>{frontmatter.excerpt}</p>
                    <TagsList reverse={reverse}>
                      {frontmatter.tags.map((tag, i) => (
                        <Tag key={`${tag}${i}`}>{tag}</Tag>
                      ))}
                    </TagsList>
                  </CardDetails>
                </Card>
                <Line />
              </div>
            )
          })
          .reverse()
        // GraphQL takes sort as a parameter in allMarkdownRemark. It's currently
        // sorted for date, so reverse shows newest posts at the top of the all
        // blogs page
      }
    }

    return (
      <Layout location={location}>
        <Wrapper>
          <Fragment>
            <SearchBar
              addTag={this.addTag}
              removeTag={this.removeTag}
              handleDrag={this.handleDrag}
              tags={tags}
              suggestions={suggestions}
            />
            {generateBlogPosts()}
          </Fragment>
        </Wrapper>
      </Layout>
    )
  }
}

export const query = graphql`
  {
    allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "//blog/.*/" } }
      sort: { fields: [frontmatter___date] }
    ) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            author
            avatar {
              childImageSharp {
                resolutions(width: 40, height: 40, quality: 80) {
                  ...GatsbyImageSharpResolutions
                }
              }
            }
            path
            excerpt
            thumbnail {
              childImageSharp {
                resolutions(width: 250, height: 250, quality: 90) {
                  ...GatsbyImageSharpResolutions_withWebp
                }
              }
            }
            tags
          }
        }
      }
    }
  }
`

export default BlogPage
