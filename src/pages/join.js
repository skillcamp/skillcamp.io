import React from 'react'
import styled from 'styled-components'
import transition from '../images/title_transition_bottom.svg'
import Layout from '../components/layout'

const Div = styled.div`
  height: 2600px;
  width: 100vw;
  position: relative;
  @media screen and (max-width: 600px) {
    height: 3750px;
  }
`

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  margin: -1px auto 0;
  background: linear-gradient(to right, #fda253, #fbc15a);
  min-height: 1500px;
  overflow: hidden;
`

const Container = styled.div`
  width: 100vw;
  max-width: 1440px;
  position: absolute;
  top: 150px;
  left: 0;
  right: 0;
  margin: 0 auto;
  z-index: 10;
  color: #222;

  display: flex;
  flex-direction: column;
  align-items: center;
`

const Title = styled.h1`
  margin: 0;
  font-size: 50px;

  @media screen and (max-width: 500px) {
    font-size: 35px;
  }
`

const TransitionBottom = styled.img`
  position: absolute;
  height: 500px;
  bottom: -1px;
  left: 0;
  z-index: 5;
  margin: 0;
  max-width: none;
`

const StripeLight = styled.span`
  position: absolute;
  height: 200px;
  width: 1400px;
  bottom: 405px;
  right: -334px;
  background-color: #fdc55c;
  transform: rotate(-45deg);
  z-index: 1;

  @media screen and (max-width: 1000px) {
    height: 200px;
    width: 600px;
    bottom: 300px;
    right: -160px;
  }
`

const StripeMed = styled.span`
  position: absolute;
  height: 200px;
  width: 1000px;
  bottom: 300px;
  right: -230px;
  background-color: #fda545;
  transform: rotate(-45deg);
  z-index: 1;

  @media screen and (max-width: 1000px) {
    height: 100px;
    width: 538px;
    bottom: 309px;
    right: -160px;
  }
`

const StripeDark = styled.span`
  position: absolute;
  height: 287px;
  width: 824px;
  bottom: 141px;
  right: -365px;
  background-color: #fd9043;
  transform: rotate(-45deg);
  z-index: 1;

  @media screen and (max-width: 1000px) {
    height: 200px;
    width: 400px;
    bottom: 140px;
    right: -180px;
  }
`
const Button = styled.a`
  width: 180px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${props => props.color};
  color: ${props => (props.font ? props.font : '#ffffff')};
  border: none;
  border-radius: 3px;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  font-size: 15px;
  text-decoration: none;
  align-self: center;
  transition: all 200ms ease;

  &:hover {
    filter: brightness(1.2);
    transform: translateY(-3px);
  }
`

const CardRow = styled.div`
  width: 100vw;
  max-width: 1440px;
  display: flex;
  flex-direction: ${props => (props.reverse ? 'row-reverse' : 'row')};
  justify-content: space-around;
  align-items: center;
  margin: 110px 0;

  @media screen and (max-width: 600px) {
    flex-direction: column;
    margin: 50px 0;
  }
`

const Card = styled.div`
  height: 400px;
  width: 700px;
  align-self: center;
  background: linear-gradient(to bottom, #444, #222);
  color: white;
  box-shadow: 0 10px 10px rgba(0, 0, 0, 0.4);
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  padding: 15px 30px;
  border-radius: 15px;
  font-weight: lighter;
  position: relative;
  overflow: hidden;
  z-index: 2;

  span {
    color: ${props => props.color};
  }

  a {
    text-decoration: none;
  }

  a:hover {
    opacity: 0.8;
  }

  @media screen and (max-width: 800px) {
    width: 500px;
  }

  @media screen and (max-width: 600px) {
    height: 550px;
    width: 400px;
    h1 {
      font-size: 30px;
      margin: 0;
    }
    p {
      font-size: 17px;
      margin: 0;
    }
  }

  @media screen and (max-width: 400px) {
    width: 320px;
  }
`

const Number = styled.h1`
  color: #333;
  font-size: 200px;
  animation: float 4s ease infinite;

  @keyframes float {
    0% {
      transform: translateY(-10px);
    }
    50% {
      transform: translateY(10px);
    }
    100% {
      transform: translateY(-10px);
    }
  }
`

class Join extends React.Component {
  render() {
    const { location } = this.props
    return (
      <Layout location={location}>
        <Div>
          <Container>
            <Title>Get Started Today!</Title>
            <CardRow>
              <Number>1</Number>
              <Card color="#22a477">
                <h1>Join Us On Slack</h1>
                <p>
                  Our slack channel is the best place to get started. Head over
                  to the{' '}
                  <a
                    href="https://skillcamp-io.slack.com/app_redirect?channel=introductions"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span>#introductions</span>
                  </a>{' '}
                  channel and say hello! We'd love to hear about your experience
                  as a developer, favorite languages and frameworks, interests,
                  or anything else you'd like to share!
                </p>
                <Button
                  href="https://join.slack.com/t/skillcamp-io/shared_invite/enQtMzgxMjM5NjU1OTU4LTUzZWIyMmNkZGUyYzBiZmI2OTEzODc4ZjI3OTg5ZGVkZTg4YTk2MWVkZmRmNzE3NjM3ZWM1M2Y4OWViMDQxN2I"
                  target="_blank"
                  color="#22a477"
                  rel="noopener noreferrer"
                >
                  Join Us On Slack
                </Button>
              </Card>
            </CardRow>

            <CardRow reverse={true}>
              <Number>2</Number>
              <Card color="#24caf0">
                <h1>Join Our Group On GitLab</h1>
                <p>
                  Our project repos are hosted on Gitlab. If you don't already
                  have an account go ahead and create one. Once you have your
                  account, head to the{' '}
                  <a
                    href="https://skillcamp-io.slack.com/app_redirect?channel=onboarding"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span>#onboarding</span>
                  </a>{' '}
                  channel, and send us your GitLab username. An admin will add
                  you to the group, to give you access to the repos, as well as
                  pull request privileges.
                </p>
                <Button
                  href="https://gitlab.com/skillcamp"
                  target="_blank"
                  color="#24caf0"
                  rel="noopener noreferrer"
                >
                  View GitLab Group
                </Button>
              </Card>
            </CardRow>

            <CardRow>
              <Number>3</Number>
              <Card color="#f24d47">
                <h1>Find a Project</h1>
                <p>
                  Skillcamp is based around collaboration, and learning through
                  doing. Check out some of our open source projects. We keep a
                  list of open issues on the GitLab issue board. Take a look
                  around and see if there is something you're interested in
                  working on. If you have an idea for your own project we can
                  help you get started! Our{' '}
                  <a
                    href="https://skillcamp-io.slack.com/app_redirect?channel=ideas"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span>#ideas</span>
                  </a>{' '}
                  channel is the place to start new projects and find a team to
                  work on it.
                </p>
                <Button
                  href="https://gitlab.com/skillcamp"
                  target="_blank"
                  color="#f24d47"
                  rel="noopener noreferrer"
                >
                  Find A Project
                </Button>
              </Card>
            </CardRow>

            <CardRow reverse={true}>
              <Number>4</Number>
              <Card color="#fbc15a">
                <h1>Start Contributing!</h1>
                <p>
                  Congrats you are now an official Skillcamp member!!
                  <span role="img" aria-label="">
                    🎉
                  </span>
                  I Improve your skills as a developer, network with others,
                  learn and discuss new technologies, write or share blog posts
                  and more!
                  <br />
                  <br />
                  If you're not sure where to start or how to contribute, head
                  over to our{' '}
                  <a
                    href="https://skillcamp-io.slack.com/app_redirect?channel=help"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <span>#help</span>
                  </a>{' '}
                  channel. Here you can ask questions or start pair programming
                  sessions with other developers!
                </p>
              </Card>
            </CardRow>
          </Container>

          <Wrapper>
            <TransitionBottom src={transition} alt="transition-bottom" />
            <StripeLight />
            <StripeMed />
            <StripeDark />
          </Wrapper>
        </Div>
      </Layout>
    )
  }
}

export default Join
