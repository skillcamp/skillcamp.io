import React from 'react'
import styled from 'styled-components'
import { graphql } from 'gatsby'
import About from '../components/About'
import Projects from '../components/Projects'
import BlogSection from '../components/BlogSection'
import TitleSection from '../components/TitleSection'
import Layout from '../components/layout'

const Wrapper = styled.div`
  margin-top: -60px;
  overflow-x: hidden;
  @media screen and (max-width: 600px) {
    margin-top: 0;
  }
`

class IndexPage extends React.Component {
  render() {
    const { data, location } = this.props
    return (
      <Layout location={location}>
        <Wrapper>
          <TitleSection />
          <About />
          <Projects />
          <BlogSection blogPosts={data.blogPosts} />
        </Wrapper>
      </Layout>
    )
  }
}

export const query = graphql`
  {
    blogPosts: allMarkdownRemark(
      limit: 4
      sort: { fields: [frontmatter___date] }
      filter: { fileAbsolutePath: { regex: "//blog/.*/" } }
    ) {
      edges {
        node {
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            path
            excerpt
            author
            thumbnail {
              childImageSharp {
                sizes(maxWidth: 200, quality: 90) {
                  ...GatsbyImageSharpSizes_noBase64
                }
              }
            }
          }
        }
      }
    }
  }
`

export default IndexPage
